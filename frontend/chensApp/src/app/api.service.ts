import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError, timer } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';

import { Item } from './class/item';
import { Register } from './class/register';
import { Registered } from './class/registered';
import { User } from './class/user';
import { Login } from './class/login';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private SERVER = "http://localhost:8080";

  loggedInStatus: boolean = false;

  constructor(private httpClient: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    console.log(error);

    // return an observable with a user friendly message
    return throwError('Error! something went wrong.');
  }

  get getLoggedInStatus(): boolean {
    return this.loggedInStatus;
  }

  setLoggedInStatus(status: boolean) {
    this.loggedInStatus = status;
  }

  getItems(): Observable<Item[]> {
    return this.httpClient.get<Item[]>(`${this.SERVER}/getItems.php`).pipe(
      catchError(this.handleError));
  }

  register(user: Register): Observable<Registered> {
    return this.httpClient.post<Registered>(`${this.SERVER}/register.php`, user).pipe(
      catchError(this.handleError));
  }

  login(user: User): Observable<Login> {
    return this.httpClient.post<Login>(`${this.SERVER}/login.php`, user);
  }

  checkUsername(username) {
    return timer(1000)
      .pipe(
        switchMap(() => {
          return this.httpClient.post<any>(`${this.SERVER}/validateUsername.php`, { username });
        })
      );
  }
}
