###Prerequisites

Install Docker and Docker-compose

### Usage
Clone the repository, navigate into the folder and run:

`sudo docker-compose up`

Open the app at [http://localhost:80](http://localhost:80)

Open phpmyadmin at [http://localhost:8000](http://localhost:8000)

#### Chen's Chinese Takeaway App

The purpose of this app is to allow customers to place orders online. 

The above command starts an Angular App and an Apache web server with a MySQL database processed by PHP.

Opening the browser loads the home page that introduces the Chen's Chinese Takeaway and Delivery. There is a side menu that can be opened by clicking the menu icon on the top right corner of the page. The side menu has a selection of buttons that navigate to other pages (Note: the About Us, Rate Us, Account and Checkout pages are still under development). The Place Orders page has an intuitive ordering user interface for selecting items and adding them to your order. The list of items are retrieved from MySQL database. The Login and Sign Up page functions like a standard login and sign up page on a website. The sign up page includes a password strength feature and an asynchronous username validation feature. The username and passwords gets processed in the backend by PHP for validation and registration using data from the MySQL database. There are no accounts available on first run. To see the validation features, first sign up an account with a username and password (for example 'username1' and 'aA1/asdf'). The the app can verify the next sign up or login against the new account. 