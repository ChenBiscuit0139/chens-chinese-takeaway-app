import { Component } from '@angular/core';
import { ApiService } from './api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nav_position: string = 'end';

  constructor(
    public apiService: ApiService,
    private router: Router) { }

  signOut(): void {
    this.apiService.setLoggedInStatus(false);
    this.router.navigate(['/home']);
  }
}
