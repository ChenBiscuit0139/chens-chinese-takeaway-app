import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup, FormGroupDirective, NgForm, AbstractControl, ValidatorFn, AsyncValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material';

import { ApiService } from '../api.service';

import { Register } from '../class/register';
import { Registered } from '../class/registered';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

export interface data {
  data: boolean;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  error = '';
  register: Register = { id: null, username: null, password: null, confirm_password: null };
  registered: Registered;

  registerForm = this.fb.group({
    username: ['', Validators.required, this.validateUsername()],
    password1: ['', Validators.required],
    password2: ['', Validators.required]
  }, { validator: this.checkPasswords });

  matcher = new MyErrorStateMatcher();

  constructor(
    private router: Router,
    private apiService: ApiService,
    private fb: FormBuilder) { }

  ngOnInit() {
  }

  get username() { return this.registerForm.get('username'); };
  get password1() { return this.registerForm.get('password1'); };
  get password2() { return this.registerForm.get('password2'); };

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password1.value;
    let confirmPass = group.controls.password2.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  onRegister(form) {
    this.apiService.register(form.value).subscribe((registered: Registered) => {
      this.registered = registered;
      if (this.registered.registered) {
        this.router.navigate(['/login']);
      }
    }, (err) => {
      console.log(err);
    })
  }

  validateUsername(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any }> => {
      return this.apiService.checkUsername(control.value)
        .pipe(
          map(res => {
            if (res.data) {
              return { 'usernameExists': true };
            }
          })
        );
    };
  }
}
