<?php
require_once 'config.php';

$items = [];

$sql = "SELECT * FROM items";

if($result = mysqli_query($connect, $sql))
{
  $i = 0;
  while($row = mysqli_fetch_assoc($result))
  {
    $items[$i]['id']    = $row['id'];
    $items[$i]['name'] = $row['name'];
    $items[$i]['category'] = $row['category'];
    $items[$i]['description'] = $row['description'];
    $items[$i]['price'] = $row['price'];
    $items[$i]['rating'] = $row['rating'];
    $i++;
  }

  echo json_encode($items);
}
else
{
  http_response_code(404);
}

// Close connection
mysqli_close($connect);
?>