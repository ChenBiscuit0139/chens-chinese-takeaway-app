export class Register {
    id: number;
    username: string;
    password: string;
    confirm_password: string;
}
