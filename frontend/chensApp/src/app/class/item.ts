export class Item {
    id: number;
    name: string;
    category: string;
    description: string;
    price: number;
    rating: number;
}
