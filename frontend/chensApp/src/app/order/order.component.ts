import { Component, OnInit } from '@angular/core';

import { ApiService } from '../api.service';

import { Item } from '../class/item';
import { Order } from '../class/order';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  constructor(private apiService: ApiService) { }

  error = '';
  items: Item[] = [
  ];

  order: Order = {
    items: [],
    total: 0.0
  };

  ngOnInit() {
    this.getItems();
  }

  getItems(): void {
    this.apiService.getItems().subscribe((items: Item[]) => {
      this.items = items;
    }, (err) => {
      this.error = err;
    });
  }

  private calculateTotal(): void {
    let total: number = 0.0;
    this.order.items.forEach(item => {
      total += +item.price;
    });
    this.order.total = total;
  }

  addItem(item): void {
    this.order.items.push(item);
    this.calculateTotal();
  }

  removeItem(item): void {
    let index = this.order.items.indexOf(item);
    this.order.items.splice(index, 1);
    this.calculateTotal();
  }

}
